const fs = require("fs");
const path = require("path");
const dotenv = require("dotenv");
const envConfig = dotenv.parse(
  fs.readFileSync(path.resolve(__dirname, `../.env`))
);
for (let k in envConfig) {
  process.env[k] = envConfig[k];
}
