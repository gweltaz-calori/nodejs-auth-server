const express = require("express");
const router = express.Router();

const RequireAuthentication = require("../middleware/RequireAuthentication");

class UserController {
  static async me(req, res) {
    res.send(req.user.toJSON());
  }
}

router.get("/@me", RequireAuthentication, UserController.me);

module.exports = router;
