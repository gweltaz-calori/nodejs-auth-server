const express = require("express");
const router = express.Router();

const Authenticator = require("../service/Authenticator");
const User = require("../model/User");
const Hasher = require("../service/Hasher");

class AuthController {
  static async login(req, res) {
    try {
      res.send({
        token: await Authenticator.authenticate(
          req.body.email,
          req.body.password
        )
      });
    } catch (e) {
      res.status(400).send(e);
    }
  }
  static async register(req, res) {
    try {
      const user = new User({
        email: req.body.email,
        password: await Hasher.hash(req.body.password)
      });
      await user.save();
      res.send({
        token: await Authenticator.authenticate(
          req.body.email,
          req.body.password
        )
      });
    } catch (e) {
      res.status(400).send(e);
    }
  }
}

router.post("/login", AuthController.login);
router.post("/register", AuthController.register);

module.exports = router;
