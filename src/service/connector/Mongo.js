const mongoose = require("mongoose");

class Mongo {
  connect() {
    mongoose.connect(
      process.env.MONGO_URL,
      { useNewUrlParser: true },
      err => {
        if (err) {
          console.log(err);
          //console.log("Can't connect to mongodb");
        }
      }
    );
  }
}

module.exports = new Mongo();
