require("../lib/env"); //load environnement variable first
require("./boot"); //initialize connections

const bodyParser = require("body-parser");
const express = require("express");
const app = express();
const router = express.Router();

app.use(bodyParser.json());

//controllers
const AuthController = require("./controller/AuthController");
const UserController = require("./controller/UserController");

app.get("/", (req, res) =>
  res.send(`Authentication Server version ${process.env.APP_VERSION}`)
);

//base routes
router.use("/auth", AuthController);
router.use("/users", UserController);

app.use("/api", router);

app.listen(process.env.APP_PORT, () =>
  console.log(`LISTENING ON PORT http://localhost:${process.env.APP_PORT}`)
);
