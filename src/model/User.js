const mongoose = require("mongoose");

const Schema = mongoose.Schema({
  name: String,
  email: {
    type: String,
    required: true
  },
  password: String
});

Schema.methods.toJSON = function() {
  return {
    email: this.email,
    _id: this._id
  };
};

module.exports = mongoose.model("User", Schema);
